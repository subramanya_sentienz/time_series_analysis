Applying Deep Learning  to modeling  time series data and comparing them to traditional econometrics based  models.


Task 1 - Applying Deep Learning Models for Stock Market Data

Data - NSE Index Data from 2000 to 2018.

A Conv1D Model is applied for above data and predicted index values.

List of Files

NSE_2000_2018.csv - NSE data from year 2000 to 2018

DATA_PREPARATION.ipynb - Ipython notebook containing above file from individual csv for each year

Exploratory_Analysis.ipynb - Exploratory Analysis of NSE Data (Ipython notebook )

Model_CNN.ipynb - CNN Model (Conv1D with Maxpooling) for NSE Data with Error Analysis and Predicted Values.

jack_demo.zip - Contains the detailed report, data,results,code, used for demo to jack. 
